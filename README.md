NostromoSoft/user-bundle
===================================

Opis
----------
Pakiet zawierjący bundle związane z obsługą użytkownika.

Instalacja
----------

#### AppKernel:

```php
    public function registerBundles() {
        $bundles = array(
            (...)
            new NostromoSoft\UserBundle\NostromoSoftUserBundle()
        );
```

#### Import routingu:  

```
nostromo_soft_base_modules_user:
    resource: "@NostromoSoftUserBundle/Controller"
    type: annotation
```

#### Konfiguracja security.yml:  

```
security:
        encoders:
            AppBundle\Entity\User: 
                algorithm:        sha1
                encode_as_base64: false
                iterations:       1

        role_hierarchy:
            ROLE_ADMIN:       ROLE_USER
            ROLE_SUPER_ADMIN: [ROLE_USER, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

        providers:
            chain_provider:
                chain:
                    providers: [NostromoSoft]
            NostromoSoft:
                id: nostromo_soft_user.user_provider
                
        firewalls:
            dev:
                pattern:  ^/(_(profiler|wdt)|css|images|js)/
                security: false

            admin_login:
                pattern:  ^/(login|przypomnienie-hasla.*)$
                anonymous: ~
                security: false
                
            admin_area:
                pattern:    ^/
                provider: NostromoSoft
                ns_form_login:
                    login_path: /login
                    check_path: /login_check
                    default_target_path: /
                    csrf_provider: security.csrf.token_manager
                    # pole którym się logujem(username/email)
                    user_login_field_name: username      
                    # pole "zapamietaj mnie"
                    remember_me: true
                    password_reset_path: forgot_request
                    # usługi obsługujące zdarzenie logowania(logują próby logowania i przekierowują)
                    failure_handler: nostromo_soft_user.login_attempt_handler
                    success_handler: nostromo_soft_user.login_attempt_handler
                remember_me:
                    # nazwa ciasteczka "zapamiętaj mnie"
                    name: admin_remember_me
                    key:      "%secret%"
                    # żywotność ciasteczka
                    lifetime: 604800
                    path:     /
                logout:
                    path:   /logout
                    target: /

        access_control:
            - { path: ^/, roles: ROLE_USER }
            - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }

```


Konfiguracja
------------

Konfiguracja bundle'a powinna zostać umieszczona w pliku `app/config/NostromoSoft/userbundle.yml`

oraz zaimportowana do `config.yml` w następujący sposób:   

```
    imports:
        (...)
        - { resource: %kernel.root_dir%/config/NostromoSoft/userbundle.yml }
```

#### Zawartość pliku konfiguracyjnego:


```
    nostromo_soft_user:
        model:
            # klasa reprezentująca encję użytkownika
            user_class: AppBundle\Entity\User
            # klasa reprezentująca managera użytkowników
            user_manager_class: AppBundle\Manager\UserManager
            # formularz dodawania i edycji użytkownika
            form_type:            NostromoSoft\UserBundle\Form\Type\User\UserType
            # formularz edycji profilu użytkownika
            profile_form_type:            NostromoSoft\UserBundle\Form\Type\User\ProfileType
        # długość historii logowania użytkownika
        login_log_length: 10
        password_reset:
            # o co pytać przy przypomnieniu hasla(username/email)
            field: email
            # service obsługujący resetowanie hasła
            handler: nostromo_soft_user.password_reset.send_token_link
        user:
            # flaga uruchamiająca wygasanie
            password_expires: true
            # liczba dni przez które hasło jest ważne
            password_lifetime: 30
            # ścieżka na którą użytkownik jest przekierowany jeżeli hasło wygasło (wymagane jest obsługiwanie parametru "id")
            change_password_route: user_change_password
            # ścieżka na którą użytkownik trafia po zmianie hasła
            change_password_success_route: dashboard
    framework:
        session:
            # żywotność ciasteczka sesji
            cookie_lifetime: 86400


```

#### Encja dla modelu User

Aplikacja korzystająca z bundle'a powinna implementować encję użytkownika rozszerzając klasę User z bundle'a lub stworzyć nową, implementującą interfejs
UserInterface i wskazać ją w konfiguracji [nostromo_soft_user.model.user_class](#zawarto-pliku-konfiguracyjnego)

Przykład:

```php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use NostromoSoft\UserBundle\Model\User as BaseUser;

    /**
     * User
     *
     * @ORM\Table()
     * @ORM\Entity
     */
    class User extends BaseUser
    {
    }

```

#### Klasa managera zarządzającego użytkownikami

Bundle implementuje domyślne manager dla skonfigurowanej encji użytkownika, można go jednak nadpisać lub rozszerzyć tworząc odpowiednią klasę
i wskazując ją w konfiguracji  [nostromo_soft_user.model.user_manager_class](#zawarto-pliku-konfiguracyjnego)

Przykład:

```php

    namespace AppBundle\Manager;

    use NostromoSoft\UserBundle\Manager\UserManager as BaseUserManager;

    class UserManager extends BaseUserManager
    {
    }

```


Funkcjonalności
----------


#### Resetowania hasła 

Mechanizm resetowania/przypomnienia hasła składa się z 2 rzeczy: 
    
    1. Akcji wyświetlającej formularz resetowania hasła

        Bundle uostępnia formularz `PasswordResetType`, który jest udostępniony jako service. Poprzez konfigurację można ustawić, które pole ma być odpytywany przy resetowaniu hasła(username lub email)
        Relizujemy to przy pomocy konfiguracji `password_reset.field`. Dzięki temu użytkownik chcący zresetować hasło zostanie zapytany o właściwe dane.

    2. Usługi handlera, który sterue procesem resetowania. Wskazać ją można przez konfigurację `password_reset.handler`

        Usługa obsługująca resetowanie hasła powinna implementować interfejs PasswordResetHandlerInterface

##### Zmiana domyślnego formularza resetowania hasła 

Jeżeli chcemy zmienić domyślny formularz zmiany/odzyskiwania hasła, musimy nadpisać typ formularza `nostromo_soft_user_password_reset_request` lub klasę obsługującą formularze dla modelu User.

##### Zmiana domyślnego sposobu obsługi przypomnienia hasła

Jeżeli chcemy zmienić sposób resetowania hasła, należy stworzyć klasę implementującą interfejs `PasswordResetHandlerInterface` oraz wskazać ją w konfiguracji `password_reset.handler`.
W funkcji `PasswordResetHandlerInterface::handle` klasa powinna zwracać obiekt `RedirectResponse` do kolejnej akcji, o ile proces odzyskiwania hasła tego wymaga, lub rzucać wyjątki
jeżeli takowe występują.

Przykład:

```php

        public function resetAction(Request $request)
        {

            $userFormManager = $this->get('nostromo_soft_user.user_form_manager');
            $form = $userFormManager->createPasswordResetRequestForm('forgot_handle');
            /* @var $handler PasswordResetHandlerInterface */
            $handler = $this->get($this->container->getParameter('nostromo_soft_user.password_reset.handler'));
            $response = $this->redirectToRoute('forgot_request');

            $form->handleRequest($request);

            try {
                // w tym miejscu handler steruje procesem odzyskiwana hasła
                // kierując na następną akcję lub do podziękowania
                // (lub nie kierując nigdzie jeżeli bp. robimy to przez REST)
                $response = $handler->handleRequest($request, $form);
            } catch (InvalidArgumentException $ex) {
                $response = $this->redirectToRoute('forgot_request');

                // przykład obsługi wyjatków poprzez przekazanie wiadomości do innej akcji poprzez sesję
                $request->getSession()->set(PasswordReset::WRONG_USERNAME, $ex->getMessage());
            }

            return $response;

        }

```

##### Domyślna konfiguracja

```

    nostromo_soft_user:
        password_reset:
            # o co pytać przy przypomnieniu hasla(username/email)
            field: email
            # service obsługujący resetowanie hasła
            handler: nostromo_soft_user.password_reset.send_token_link

```

#### Wymuszanie siły hasła

Do tego używany jest bundle [rollerworks/password-strength-bundle](https://github.com/rollerworks/PasswordStrengthBundle)

Domyślnie wymuszanie siły hasłą jest wyłączone. Możemy ustalić konkretne wymagania dotyczące hasła w encji implementującej model użytkownika, 
wystarczy odpowiednia adnotacja na polu reprezentującym niezakodowane hasło użytkownika:

```php

    use Rollerworks\Bundle\PasswordStrengthBundle\Validator\Constraints as RollerworksPassword;
    
    (...)

    /**
     * @RollerworksPassword\PasswordRequirements(requireLetters=true, requireNumbers=true, requireCaseDiff=true, requireSpecialCharacter=false
      tooShortMessage = "Hasło musi zawierać przynajmniej {{length}} znaków.",
      missingLettersMessage = "Hasło musi zawierać przynajmniej jedną literę.",
      requireCaseDiffMessage = "Hasło musi zawierać duże i małe litery.",
      missingNumbersMessage = "Hasło musi zawierać przynajmniej jedną liczbę.",
      missingSpecialCharacterMessage = "Hasło musi zawierać przynajmniej jeden znak specjalny."
     )
     * 
     */
    protected $plainPassword;

```

lub wymusić "siłę"

```php

    use Rollerworks\Bundle\PasswordStrengthBundle\Validator\Constraints as RollerworksPassword;
    
    (...)

    /**
     * @RollerworksPassword\PasswordStrength(minLength=7, minStrength=3)
     */
    protected $password;

```
#### Wygasanie hasła

Mechanizm wygasania hasła polega na przechowywaniu w użytkowniku daty wygaśnięcia hasła i przekierowaniu na akcję zmiany hasła
po jego wygaśnięciu

Jeżeli funkcjonalność działa, nowi użytkownicy są proszeni o zmianę hasła po 1-szym logowaniu

##### Domyślna konfiguracja

```

    nostromo_soft_user:
        user:
            # flaga uruchamiająca wygasanie
            password_expires: true
            # liczba dni przez które hasło jest ważne
            password_lifetime: 30
            # ścieżka na którą użytkownik jest przekierowany jeżeli hasło wygasło
            change_password_route: user_change_password
            # ścieżka na którą użytkownik trafia po zmianie hasła
            change_password_success_route: dashboard

```
