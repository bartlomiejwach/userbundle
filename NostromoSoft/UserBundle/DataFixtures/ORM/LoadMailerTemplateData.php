<?php

/**
 * Copyright (c) 2014 by Nostromo Soft.
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */
namespace NostromoSoft\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadMailerTemplateData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        // Pobieranie serwisu TemplateManager
        $templateManager = $this->container->get('nostromo_soft_base_modules_mailer.template_manager');

        // Nowy rekord
        $newEntry = $templateManager->create(
                        'Przypomnienie hasła',
                        'forgot_password',
                        'Przypomnienie hasła',
                        'test@yahoo.pl',
                        'Nostromo Soft',
                        'twig_template',
                        '',
                        'NostromoSoftUserBundle:Mail:forgot_password.html.twig'
                );
        $manager->persist($newEntry);

        // Zapisywanie danych do bazy
        $manager->flush();
    }
}
