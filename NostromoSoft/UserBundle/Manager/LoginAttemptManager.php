<?php

/*
 * Copyright (c) 2016 by Nostromo Soft.
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.<br> *
 */

namespace NostromoSoft\UserBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use InvalidArgumentException;
use NostromoSoft\Base\Modules\BaseBundle\Manager\CrudObjectManager;
use NostromoSoft\Base\Modules\DataBundle\Manager\PagerManager;
use NostromoSoft\UserBundle\Entity\LoginAttempt;
use NostromoSoft\UserBundle\Model\UserInterface;

/**
 * Manager zarządzający logowaniem prób logowania użytkownika.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class LoginAttemptManager extends CrudObjectManager
{
    protected $logLength;

    public function __construct(Registry $doctrineRegistry, PagerManager $pagerManager, $logLength = 100)
    {
        parent::__construct('NostromoSoftUserBundle:LoginAttempt', $doctrineRegistry, $pagerManager);

        $this->logLength = $logLength;
    }

    public function create()
    {
        return new LoginAttempt();
    }

    /**
     * Zapisuje zdarzenie w bazie jedocześnie kontrolując długoś loga dla użytkownika.
     *
     * @param LoginAttempt $la
     */
    public function log(LoginAttempt $la)
    {
        if (!$la->getUserId()) {
            throw new InvalidArgumentException('Brak ustawionego id użutkownika dla zdarzenia logowania');
        }

        if ($this->logLength > 0) {
            $numOfEntriesToKeep = $this->logLength - 1;
            $em = $this->getEntityManager();

            $entriesToRemove = $this->repository->findBy(
                    array('userId' => $la->getUserId()), array('loggedAt' => 'DESC'), null, $numOfEntriesToKeep)
            ;

            foreach ($entriesToRemove as $oldLog) {
                $em->remove($oldLog);
            }

            $em->persist($la);

            $em->flush();
        }
    }

    /**
     * Zwraca listę ostatnich prób logowania dla usera.
     *
     * @param \NostromoSoft\UserBundle\Manager\UserInterface|UserInterface $user
     * @param int                                                          $limit liczba prób logowania do zwrócenia
     *
     * @return array
     */
    public function getLogForUser(UserInterface $user, $limit = 10)
    {
        return $this->repository->findBy(array('userId' => $user->getId()), array('loggedAt' => 'DESC'), $limit);
    }
}
