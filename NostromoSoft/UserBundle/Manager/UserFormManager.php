<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All rights reserved.
 */

namespace NostromoSoft\UserBundle\Manager;

use NostromoSoft\Base\Modules\BaseBundle\Manager\CrudObjectFormManager;
use NostromoSoft\Base\Modules\BaseBundle\Manager\CrudObjectManager;
use NostromoSoft\UserBundle\Form\Type\User\PasswordType;
use NostromoSoft\UserBundle\Form\Type\User\ProfilePasswordType;
use NostromoSoft\UserBundle\Model\UserInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Manager dla formularz związanych z użytkownikiem.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class UserFormManager extends CrudObjectFormManager
{
    protected $userClass;
    protected $formType;
    protected $profileFormType;

    public function __construct(CrudObjectManager $crudObjectManager, FormFactory $formFactory, $router, $userClass, $formType, $profileFormType)
    {
        parent::__construct($crudObjectManager, $formFactory, $router);

        $this->userClass = $userClass;
        $this->formType = $formType;
        $this->profileFormType = $profileFormType;
    }

    /**
     * Tworzy formularz zapytania o odzyskanie hasła.
     *
     * @param string $action
     *
     * @return Form
     */
    public function createPasswordResetRequestForm($action)
    {
        $form = $this->formFactory->create('nostromo_soft_user_password_reset_request', array(), array(
            'action' => $this->getRouter()->generate($action, array(), UrlGeneratorInterface::ABSOLUTE_PATH),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Tworzy formularz restowania hasła.
     *
     * @param UserInterface $user
     * @param string        $action
     *
     * @return Form
     */
    public function createPasswordResetForm(UserInterface $user, $action)
    {
        $form = $this->formFactory->create('nostromo_soft_user_password_reset', $user, array(
            'action' => $this->getRouter()->generate($action, array('token' => $user->getConfirmationToken())),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Tworzenie domyślnego formularza dla modelu.
     *
     * @param $action
     * @param array $actionParameters
     * @param null  $object
     *
     * @return
     */
    public function createForm($action, $actionParameters = array(), $object = null)
    {
        return $this->createUserEditForm($this->formType, $action, $actionParameters, $object);
    }

    /**
     * Tworzenie Formularza edycji profilu.
     *
     * @param $action
     * @param array $actionParameters
     * @param null  $object
     *
     * @return
     */
    public function createProfileForm($action, $actionParameters = array(), $object = null)
    {
        return $this->createUserEditForm($this->profileFormType, $action, $actionParameters, $object);
    }

    /**
     * Tworzy dany formularz służący do edycji użytkownika.
     *
     * @param $formType
     * @param type  $action
     * @param array $actionParameters
     * @param type  $object
     *
     * @return
     *
     * @internal param type $parameters
     */
    protected function createUserEditForm($formType, $action, $actionParameters = array(), $object = null)
    {
        if (class_exists($formType)) {
            $form = new $formType();

            return $this->createFormObject($form, $action, $actionParameters, $object);
        } else {
            if (!$object) {
                $object = $this->getObjectManager()->create();
            }

            $form = $this->formFactory->create($formType, $object, array(
                'action' => $this->getRouter()->generate($action, $actionParameters, UrlGeneratorInterface::ABSOLUTE_PATH),
                'method' => 'POST',
                'data_class' => $this->userClass,
            ));

            return $form;
        }
    }

    /**
     * Tworzy formularz zmiany hasła.
     *
     * @param type       $action
     * @param array|type $parameters
     * @param type       $object
     *
     * @return type
     */
    public function createChangePasswordForm($action, $parameters = array(), $object = null)
    {
        $formType = new PasswordType();

        $form = $this->createFormObject($formType, $action, $parameters, $object);

        return $form;
    }

    /**
     * Tworzy formularz zmiany hasła.
     *
     * @param type       $action
     * @param array|type $parameters
     * @param type       $object
     *
     * @return type
     */
    public function createChangeProfilePasswordForm($action, $parameters = array(), $object = null)
    {
        $formType = new ProfilePasswordType();

        $form = $this->createFormObject($formType, $action, $parameters, $object);

        return $form;
    }
}
