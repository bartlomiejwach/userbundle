<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\Manager;

use DateTime;
use Doctrine\Bundle\DoctrineBundle\Registry;
use NostromoSoft\Base\Modules\BaseBundle\Manager\CrudObjectManager;
use NostromoSoft\Base\Modules\DataBundle\Manager\PagerManager;
use NostromoSoft\UserBundle\Model\UserInterface;
use NostromoSoft\UserBundle\User\PasswordUpdaterInterface;

/**
 * UserManager - Menadżer do zarządzania obiektami użytkowników.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class UserManager extends CrudObjectManager
{
    protected $passwordUpdater;
    protected $userClass;

    public function __construct(Registry $doctrineRegistry, PagerManager $pagerManager, PasswordUpdaterInterface $passwordUpdater, $userClass)
    {
        parent::__construct($userClass, $doctrineRegistry, $pagerManager);

        $this->passwordUpdater = $passwordUpdater;

        $this->userClass = $this->getEntityManager()->getClassMetadata($userClass)->getName();
    }

    /**
     * Zwraca użytkownika na podstawie nazy użytkownika.
     *
     * @param string $username
     *
     * @return null|UserInterface
     */
    public function getByUsername($username)
    {
        return $this->repository->findOneByUsername($username);
    }

    /**
     * Zwraca użytkownika na podstawie adresu email.
     *
     * @param string $email
     *
     * @return null|UserInterface
     */
    public function getByEmail($email)
    {
        return $this->repository->findOneByEmail($email);
    }

    /**
     * Zwraca użytkownika znalezionego na podstawie wartości dla danego pola.
     *
     * @param string $fieldName
     * @param string$fieldValue
     *
     * @return null|UserInterface
     */
    public function getByField($fieldName, $fieldValue)
    {
        return $this->repository->findOneBy(array(
                $fieldName => $fieldValue,
        ));
    }

    public function changeForgotPassword(UserInterface $user)
    {
        $user->setConfirmationToken(null);
        $user->setPasswordRequestedAt(null);

        $this->update($user);
    }

    /**
     * Wycina początek adresu email.
     *
     * @param $user UserInterface
     *
     * @return string
     */
    public function getObfuscatedEmail(UserInterface $user)
    {
        $email = $user->getEmail();
        if (false !== $pos = strpos($email, '@')) {
            $email = '...'.substr($email, $pos);
        }

        return $email;
    }

    public function create($username = null, $password = null, $email = null, $isAdmin = false)
    {
        /* @var $user UserInterface */
        $user = new $this->userClass();
        $user->setEmail($email);
        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setIsAdmin($isAdmin);
        $user->setIsActive($isAdmin ? true : false);

        return $user;
    }

    public function update($user, $andFlush = true, $emAlias = null)
    {
        $this->updatePassword($user);

        parent::update($user, $andFlush, $emAlias);
    }

    /**
     * Aktualizuje hasło użytkownika oraz dane dotyczące wygaśnięcia itd.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    protected function updatePassword(UserInterface $user)
    {
        $this->passwordUpdater->updatePassword($user);

        return $user;
    }

    public function setForgotToken(UserInterface $user)
    {
        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken(sha1(uniqid()));
            $user->setPasswordRequestedAt(new DateTime());
        }

        return $user;
    }

    /**
     * Loguje zdarzenie udanego logowania.
     *
     * @param UserInterface $user
     */
    public function handleLoginSuccess(UserInterface $user)
    {
        $em = $this->getEntityManager();

        $user->setLastLogin(new DateTime());
        $user->setLoginCount($user->getLoginCount() + 1);

        $em->persist($user);
        $em->flush();
    }
}
