<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All rights reserved.
 */

namespace NostromoSoft\UserBundle\Form\Type\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('username')
                ->add('name')
                ->add('surname')
                ->add('email')
                ->add('isActive', null, array('required' => false))
                ->add('isAdmin', null, array('required' => false))
        ;

        if (isset($options['data'])) {
            if (!$options['data']->getId()) {
                $builder
                        ->add('plainPassword', 'repeated', array(
                            'type' => 'password',
                            'invalid_message' => 'Hasła nie pasują do siebie',
                            'required' => true,
                            'first_options' => array('label' => 'Hasło'),
                            'second_options' => array('label' => 'Powtórz hasło'),
                ));
            }
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'validation_groups' => function (FormInterface $form) {
                $data = $form->getData();
                $validationGroups = ['Default'];

                // hasło jest walidowane tylko przy odawaniu usera
                // przy edycji nie ma tego pola, zgodnie z wymaganiami
                if (is_object($data) && !$data->getId()) {
                    $validationGroups[] = 'Password';
                }

                return $validationGroups;
            }, ));
    }

            /**
             * @return string
             */
            public function getName()
            {
                return 'user';
            }
}
