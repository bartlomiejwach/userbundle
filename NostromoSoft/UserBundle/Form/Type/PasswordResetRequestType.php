<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All rights reserved.
 */

namespace NostromoSoft\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Formularz odpytujący użytkownika o dane do przypomnienia hasła.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class PasswordResetRequestType extends AbstractType
{
    protected $passwordResetField;

    /**
     * @param string $passwordResetField nazwa pola, które odpytujemy, żeby znależć użytkownika
     */
    public function __construct($passwordResetField)
    {
        $this->passwordResetField = $passwordResetField;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add($this->passwordResetField, 'text', [
                    'label' => sprintf('password_reset.request.%s', $this->passwordResetField),
                    'constraints' => array(new Type(array('type' => 'string'))),
                    ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'ns_user',
        ));
    }

    public function getName()
    {
        return 'nostromo_soft_user_password_reset_request';
    }
}
