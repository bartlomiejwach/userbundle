<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All rights reserved.
 */

namespace NostromoSoft\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Formularz faktycznej zmiany hasła przy odzyskiwaniu/resecie.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class PasswordResetType extends AbstractType
{
    protected $class;

    /**
     * @param string $class Klasa reprezentująca użytkownika
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'Hasła nie pasują do siebie',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention' => 'resetting',
        ));
    }

    public function getName()
    {
        return 'nostromo_soft_user_password_reset';
    }
}
