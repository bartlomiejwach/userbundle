<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All rights reserved.
 */

namespace NostromoSoft\UserBundle\PasswordReset;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interfejs klasy implementującej mechanizm resetowania hasła.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
interface PasswordResetHandlerInterface
{
    /**
     * Funkcja osbługuje zapytanie z formularza i wykonuje odpowiednie akcje.
     *
     * @param Request $request
     * @param Form    $form
     *
     * @return
     */
    public function handleRequest(Request $request, Form $form);
}
