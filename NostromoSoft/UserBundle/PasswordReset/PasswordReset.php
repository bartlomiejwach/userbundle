<?php
/*
 * Copyright (c) 2016 by Nostromo Soft.
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.<br> *
 */

namespace NostromoSoft\UserBundle\PasswordReset;

/**
 * Klasa przechowyje identyfikatory powiadomień związanych z resetowaniem hasła.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
final class PasswordReset
{
    const WRONG_USERNAME = '_password_reset.wrong_username';
}
