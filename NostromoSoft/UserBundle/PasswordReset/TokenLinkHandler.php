<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All rights reserved.
 */

namespace NostromoSoft\UserBundle\PasswordReset;

use Exception;
use InvalidArgumentException;
use NostromoSoft\Base\Modules\MailerBundle\Manager\MailerManager;
use NostromoSoft\UserBundle\Manager\UserManager;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Obsługuje proces resetowania hasła za pomocą linka z tokenem.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class TokenLinkHandler implements PasswordResetHandlerInterface
{
    protected $userManager;
    protected $requestField;
    protected $mailer;
    protected $router;

    public function __construct(UserManager $userManager, $requestField, MailerManager $mailer, RouterInterface $router)
    {
        $this->userManager = $userManager;
        $this->requestField = $requestField;
        $this->mailer = $mailer;
        $this->router = $router;
    }

    public function handleRequest(Request $request, Form $form)
    {
        $value = $form->get($this->requestField)->getData();

        $user = $this->userManager->getByField($this->requestField, $value);

        if (!$user) {
            throw new InvalidArgumentException('Nie istnieje taki użytkownik');
        }

        $this->userManager->setForgotToken($user);
        $confirmationUrl = $this->router->generate('forgot_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

        try {
            $this->mailer->sendMessage('forgot_password', $user->getEmail(), ['email' => $user->getEmail(), 'confirmationUrl' => $confirmationUrl]);
        } catch (Exception $e) { // znalezc wlasciwy exception
            return new RedirectResponse($this->router->generate('forgot_request'));
        }

        $this->userManager->update($user);

        return new RedirectResponse($this->router->generate('forgot_email_sent', array('email' => $this->userManager->getObfuscatedEmail($user))));
    }
}
