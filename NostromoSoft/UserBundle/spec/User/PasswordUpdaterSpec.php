<?php

namespace spec\NostromoSoft\UserBundle\User;

use NostromoSoft\UserBundle\Model\UserInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Security\Core\Encoder\BasePasswordEncoder;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class PasswordUpdaterSpec extends ObjectBehavior
{
    public function it_is_initializable(EncoderFactoryInterface $encoderFactory)
    {
        $this->beConstructedWith($encoderFactory, /* passwordExpires */ false, /* passwordLifeTime */ 0);

        $this->shouldHaveType('NostromoSoft\UserBundle\User\PasswordUpdater');
    }

    public function it_should_encode_users_password(EncoderFactoryInterface $encoderFactory, BasePasswordEncoder $encoder, UserInterface $user)
    {
        $this->beConstructedWith($encoderFactory, /* passwordExpires */ false, /* passwordLifeTime */ 0);

        $encoderFactory->getEncoder($user)->willReturn($encoder);
        $user->getPlainPassword()->willReturn('asd');
        $encoder->encodePassword('asd', 'asd')->willReturn('encoded_password');

        $user->getSalt()->willReturn('asd');
        $user->setPassword('encoded_password')->shouldBeCalled();
        $user->setPasswordChangedAt(new \DateTime())->shouldBeCalled();
        $user->eraseCredentials()->shouldBeCalled();

        $this->updatePassword($user);
    }

//    function it_should_change_password_expiration_date_if_that_feature_is_enabled(EncoderFactoryInterface $encoderFactory, BasePasswordEncoder $encoder, UserInterface $user)
//    {
//        $passwordLifeTime = 30;
//        $this->beConstructedWith($encoderFactory, /* passwordExpires */ true, /* passwordLifeTime = 30 days */ $passwordLifeTime);

//        $encoderFactory->getEncoder($user)->willReturn($encoder);
//        $user->getPlainPassword()->willReturn('asd');
//        $encoder->encodePassword('asd', 'asd')->willReturn('encoded_password');

//        $user->getSalt()->willReturn('asd');
//        $user->setPassword('encoded_password')->shouldBeCalled();
//        $user->setPasswordChangedAt(new \DateTime())->shouldBeCalled();
//        $user->eraseCredentials()->shouldBeCalled();

//        $user->setPasswordExpiresAt(new \DateTime("@" . time()))->shouldBeCalleD();

//        $this->updatePassword($user);
//    }
}
