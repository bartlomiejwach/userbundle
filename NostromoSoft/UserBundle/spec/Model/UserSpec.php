<?php

namespace spec\NostromoSoft\UserBundle\Model;

use PhpSpec\ObjectBehavior;

class UserSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('NostromoSoft\UserBundle\Model\User');
    }

    public function it_recognizes_that_password_expired()
    {
        $this->setPasswordExpiresAt(new \DateTime('-1 day'));
        $this->isPasswordExpired()->shouldReturn(true);
    }

    public function it_recognizes_that_password_didnt_expire()
    {
        $this->setPasswordExpiresAt(new \DateTime('+1 day'));
        $this->isPasswordExpired()->shouldReturn(false);
    }

    // to jest test dla UserManager::updatePassword
    // zostawiam do przeniesienia stąd
//    function it_changes_password_expiration_date_when_password_changes()
//    {
//        $passwordExpirationDateBeforePasswordWasSet = new \DateTime("-1 day");

//        $this->setPasswordExpiresAt($passwordExpirationDateBeforePasswordWasSet);
//        $this->setPassword('asd');
//        $this->getPasswordExpiresAt()->shouldNotBe($passwordExpirationDateBeforePasswordWasSet);
//    }
}
