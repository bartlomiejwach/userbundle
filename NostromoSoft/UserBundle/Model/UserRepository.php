<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\Model;

use NostromoSoft\Base\Modules\BaseBundle\ORM\CrudRepository;

/**
 * Bazowe repozytorium dla modelu użytkownika.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class UserRepository extends CrudRepository
{
}
