<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.*
 */

namespace NostromoSoft\UserBundle\Model;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Serializable;

/**
 * Interfejs dla klasy reprezentującej użytkownika.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
interface UserInterface extends AdvancedUserInterface, Serializable
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username);

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name);

    /**
     * Get name.
     *
     * @return string
     */
    public function getName();

    /**
     * Set surname.
     *
     * @param string $surname
     *
     * @return User
     */
    public function setSurname($surname);

    /**
     * Get surname.
     *
     * @return string
     */
    public function getSurname();

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password);

    /**
     * Set plain password.
     *
     * @param string $plainPassword
     *
     * @return User
     */
    public function setPlainPassword($plainPassword);

    /**
     * Get plain password.
     *
     * @return string
     */
    public function getPlainPassword();

    /**
     * Set lastLogin.
     *
     * @param string $lastLogin
     *
     * @return User
     */
    public function setLastLogin($lastLogin);

    /**
     * Get lastLogin.
     *
     * @return string
     */
    public function getLastLogin();

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return User
     */
    public function setIsActive($isActive);

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive();

    /**
     * Set isAdmin.
     *
     * @param bool $isAdmin
     *
     * @return User
     */
    public function setIsAdmin($isAdmin);

    /**
     * Get isAdmin.
     *
     * @return bool
     */
    public function getIsAdmin();

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email);

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function __toString();

    /**
     * Set salt.
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt);

    /**
     * Get salt.
     *
     * @return string
     */
    public function getSalt();

    public function eraseCredentials();

    public function getRoles();

    public function getFullname();

    public function setConfirmationToken($confirmationToken);

    public function getConfirmationToken();

    public function getPasswordRequestedAt();

    public function setPasswordRequestedAt($passwordRequestedAt);

    public function setLoginCount($loginCount);

    public function getLoginCount();

    public function setIsLocked($isLocked);

    public function getIsLocked();

    public function setPasswordExpiresAt($dateTime);

    public function getPasswordExpiresAt();

    public function isPasswordExpired();

    public function getPasswordChangedAt();

    public function setPasswordChangedAt($passwordChangedAt);

    /**
     * @see \Serializable::serialize()
     */
    public function serialize();

    /**
     * @see \Serializable::unserialize()
     *
     * @param string $serialized
     */
    public function unserialize($serialized);
}
