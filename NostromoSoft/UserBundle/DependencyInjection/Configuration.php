<?php

namespace NostromoSoft\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('nostromo_soft_user');

        $rootNode
                ->children()
                    ->arrayNode('model')
                        ->children()
                            ->scalarNode('user_class')->cannotBeEmpty()->isRequired()->end()
                            ->scalarNode('user_manager_class')->defaultValue('')->end()
                            ->scalarNode('form_type')->defaultValue('NostromoSoft\\UserBundle\\Form\\Type\\User\\UserType')->end()
                            ->scalarNode('profile_form_type')->defaultValue('NostromoSoft\\UserBundle\\Form\\Type\\User\\ProfileType')->end()
                        ->end()
                    ->end()
                    ->integerNode('login_log_length')->defaultValue(10)->end()
                    ->arrayNode('user')->addDefaultsIfNotSet()
                        ->children()
                            ->booleanNode('password_expires')->defaultValue(false)->end()
                            ->integerNode('password_lifetime')->defaultValue('30')->end() // 30 dni
                            ->scalarNode('change_password_route')->defaultValue('user_profile_change_password')->end()
                            ->scalarNode('change_password_success_route')->defaultValue('dashboard')->end()
                        ->end()
                    ->end()
                    ->arrayNode('password_reset')->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('field')
                                ->validate()
                                ->ifNotInArray(array('username', 'email'))
                                    ->thenInvalid('Niedozwolone pole odpytywane przy resetowaniu hasła "%s"')
                                ->end()
                                ->defaultValue('username')
                            ->end()
                            ->scalarNode('handler')->defaultValue('nostromo_soft_user.password_reset.send_token_link')->end()
                        ->end()
                    ->end()
                ->end()

        ;

        return $treeBuilder;
    }
}
