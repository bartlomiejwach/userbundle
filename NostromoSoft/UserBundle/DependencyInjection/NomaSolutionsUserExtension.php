<?php

namespace NostromoSoft\UserBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class NomaSolutionsUserExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configNamespace = 'nostromo_soft_user';
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        if (isset($config['model']['user_class'])) {
            $container->setParameter("$configNamespace.user_class", $config['model']['user_class']);
        }

        $container->setParameter("$configNamespace.user_manager.class", $config['model']['user_manager_class']);
        $container->setParameter("$configNamespace.form_type", $config['model']['form_type']);
        $container->setParameter("$configNamespace.profile_form_type", $config['model']['profile_form_type']);
        $container->setParameter("$configNamespace.login_log_length", $config['login_log_length']);
        $container->setParameter("$configNamespace.password_reset.field", $config['password_reset']['field']);
        $container->setParameter("$configNamespace.password_reset.handler", $config['password_reset']['handler']);
        $container->setParameter("$configNamespace.user.password_expires", $config['user']['password_expires']);
        $container->setParameter("$configNamespace.user.password_lifetime", $config['user']['password_lifetime']);
        $container->setParameter("$configNamespace.user.change_password_route", $config['user']['change_password_route']);
        $container->setParameter("$configNamespace.user.change_password_success_route", $config['user']['change_password_success_route']);
    }
}
