<?php

namespace NostromoSoft\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LoginAttempt.
 *
 * @ORM\Table(name="login_attempt")
 * @ORM\Entity(repositoryClass="NostromoSoft\UserBundle\Entity\LoginAttemptRepository")
 */
class LoginAttempt
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="loggedAt", type="datetime")
     */
    private $loggedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_successfull", type="boolean")
     */
    private $isSuccessfull;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return LoginAttempt
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set loggedAt.
     *
     * @param \DateTime $loggedAt
     *
     * @return LoginAttempt
     */
    public function setLoggedAt($loggedAt)
    {
        $this->loggedAt = $loggedAt;

        return $this;
    }

    /**
     * Get loggedAt.
     *
     * @return \DateTime
     */
    public function getLoggedAt()
    {
        return $this->loggedAt;
    }

    /**
     * Set iSuccessfull.
     *
     * @param bool $isSuccessfull
     *
     * @return LoginAttempt
     */
    public function setIsSuccessfull($isSuccessfull)
    {
        $this->isSuccessfull = $isSuccessfull;

        return $this;
    }

    /**
     * Get iSuccessfull.
     *
     * @return bool
     */
    public function getIsSuccessfull()
    {
        return $this->isSuccessfull;
    }
}
