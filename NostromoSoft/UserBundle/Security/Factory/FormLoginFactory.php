<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.*
 */

namespace NostromoSoft\UserBundle\Security\Factory;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\FormLoginFactory as BaseFormLoginFactory;

/**
 * FormLoginFactory - przesłonięcie podstawowego FormLoginFactory z rozszerzeniem o metody NostromoSoft.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class FormLoginFactory extends BaseFormLoginFactory
{
    public function __construct()
    {
        parent::__construct();
        $this->addOption('user_login_field_name', 'username');
        $this->addOption('password_reset_path', null);
    }

    public function getKey()
    {
        return 'ns_form_login';
    }

    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $provider = 'nostromo_soft_user.authentication_provider';
        $providerId = $provider.'.'.$id;

        $container
            ->setDefinition($providerId, new DefinitionDecorator($provider))
            ->replaceArgument(1, $id) // Provider Key
            ->replaceArgument(2, new Reference($userProviderId)) // User Provider
            ->replaceArgument(5, $config['user_login_field_name'])
            ->replaceArgument(6, $config['remember_me'])
            ->replaceArgument(7, $config['password_reset_path'])
        ;

        return $providerId;
    }
}
