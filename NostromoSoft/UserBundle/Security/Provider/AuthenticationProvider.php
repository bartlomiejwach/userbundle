<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.*
 */

namespace NostromoSoft\UserBundle\Security\Provider;

use Symfony\Component\Security\Core\Authentication\Provider\DaoAuthenticationProvider;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationServiceException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use NostromoSoft\UserBundle\Model\UserInterface as NSUser;

/**
 * AuthenticationProvider - przesłonięcie podstawowego DaoAuthenticationProvidera z rozszerzeniem o metody NostromoSoft.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class AuthenticationProvider extends DaoAuthenticationProvider
{
    /** @var EncoderFactory */
    protected $encoderFactory;
    protected $userProvider;
    protected $loginFieldName;
    protected $isRememberMeAware;
    protected $resetPasswordPath;

    public function __construct(UserCheckerInterface $userChecker, $providerKey, UserProviderInterface $userProvider, EncoderFactoryInterface $encoderFactory, $hideUserNotFoundExceptions = true, $loginFieldName = 'username', $isRememberMeAware = false, $resetPasswordPath = null)
    {
        parent::__construct($userProvider, $userChecker, $providerKey, $encoderFactory, $hideUserNotFoundExceptions);

        $this->encoderFactory = $encoderFactory;
        $this->userProvider = $userProvider;

        $this->loginFieldName = $loginFieldName;
        $this->isRememberMeAware = $isRememberMeAware;
        $this->resetPasswordPath = $resetPasswordPath;
    }

    protected function checkAuthentication(UserInterface $user, UsernamePasswordToken $token)
    {
        $currentUser = $token->getUser();
        if ($currentUser instanceof NSUser) {
            if ($currentUser->getPassword() !== $user->getPassword()) {
                throw new BadCredentialsException('The credentials were changed from another session.');
            }
        } else {
            if ('' === ($presentedPassword = $token->getCredentials())) {
                throw new BadCredentialsException('Hasło nie może być puste.');
            }

            if (!$this->encoderFactory->getEncoder($user)->isPasswordValid($user->getPassword(), $presentedPassword, $user->getSalt())) {
                throw new BadCredentialsException('Niewłaściwe hasło.');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function retrieveUser($loginFieldValue, UsernamePasswordToken $token)
    {
        $user = $token->getUser();
        if ($user instanceof UserInterface) {
            return $user;
        }

        try {
            $user = $this->userProvider->loadUserByField($this->loginFieldName, $loginFieldValue);

            if (!$user instanceof UserInterface) {
                throw new AuthenticationServiceException('The user provider must return a UserInterface object.');
            }

            return $user;
        } catch (UsernameNotFoundException $notFound) {
            $notFound->setUsername($loginFieldValue);
            throw $notFound;
        } catch (\Exception $repositoryProblem) {
            $ex = new AuthenticationServiceException($repositoryProblem->getMessage(), 0, $repositoryProblem);
            $ex->setToken($token);
            throw $ex;
        }
    }

    /**
     * Zwraca nazwę pola, po którym następuje rozpoznanie użytkownika.
     *
     * @return string
     */
    public function getLoginFieldName()
    {
        return $this->loginFieldName;
    }

    /**
     * Zwraca info o tym, czy provider korzysta z funkcji "remember me".
     *
     * @return bool
     */
    public function isRememberMeAware()
    {
        return $this->isRememberMeAware;
    }

    /**
     * Zwraca info o tym, czy formularz logowania posiada przycisk "przypomnij/resetu haslo".
     *
     * @return bool
     */
    public function getResetPasswordPath()
    {
        return $this->resetPasswordPath;
    }
}
