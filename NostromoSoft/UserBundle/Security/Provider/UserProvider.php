<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.*
 */

namespace NostromoSoft\UserBundle\Security\Provider;

use NostromoSoft\Base\Modules\BaseBundle\Manager\CrudObjectManager;
use NostromoSoft\UserBundle\Manager\UserManager;
use NostromoSoft\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface as SecurityUserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * UserProvider - Klasa wykorzystywana przez moduły security Symfony w celu pobrania obiektu użytkownika z bazy.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class UserProvider implements UserProviderInterface
{
    private $userManager;
    protected $userClass;

    public function __construct(CrudObjectManager $userManager, $userClass)
    {
        $this->userManager = $userManager;
        $this->userClass = $userClass;
    }

    /**
     * @param string $username
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException
     */
    public function loadUserByUsername($username)
    {
        try {
            $user = $this->loadUserByField('username', $username);
        } catch (Exception $ex) {
            throw new UsernameNotFoundException(sprintf('Użytkownik "%s" nie istnieje.', $username));
        }

        return $user;
    }

    public function loadUserByField($fieldName, $fieldValue)
    {
        $user = $this->userManager->getByField($fieldName, $fieldValue);

        if (!$user) {
            throw new UsernameNotFoundException(sprintf('Nie można znaleźć użytkownika dla danych: %s = %s.', $fieldName, $fieldValue));
        }

        return $user;
    }

    public function refreshUser(SecurityUserInterface $user)
    {
        if (!$user instanceof UserInterface) {
            throw new UnsupportedUserException(sprintf('Klasa "%s" nie jest wspierana.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === $this->userClass;
    }
}
