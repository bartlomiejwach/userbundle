<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.*
 */

namespace NostromoSoft\UserBundle\Security\Listener;

use NostromoSoft\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * LoginEventListener - Listener nasłuchujący informacji o próbie logowania.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class LoginEventListener
{
    protected $userManager;
    protected $authorizationChecker;

    public function __construct($userManager, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->userManager = $userManager;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Obsługuje zdarzeni udanego logowania.
     *
     * IS_AUTHENTICATED_FULLY - logowanie przez formularz
     * IS_AUTHENTICATED_REMEMBERED - logowanie przez ciasteczko lub formularz
     *
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $token = $event->getAuthenticationToken();
        if ($token && $token->getUser() instanceof UserInterface) {

            // nowe logowanie przez formularz
            if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
                $user = $token->getUser();
                $um = $this->userManager;
                $um->handleLoginSuccess($user);
            }
        }
    }
}
