<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\Security\Listener;

use NostromoSoft\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Listener sprawdzający, czy hasło nie wygasło.
 *
 * @see http://www.iliveinperego.com/2013/02/fosuserbundle-force-user-change-password/
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class PasswordExpiredListener
{
    protected $authorizationChecker;
    protected $tokenStorage;
    protected $router;
    protected $session;
    protected $passwordExpires;
    protected $redirectRoute;

    public function __construct(RouterInterface $router, TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $authorizationChecker, SessionInterface $session, $passwordExpires, $redirectRoute)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
        $this->session = $session;
        $this->passwordExpires = $passwordExpires;
        $this->redirectRoute = $redirectRoute;
    }

    /**
     * Sprawdza czy wygasanie est włączone i czy użytkownik jest za firewallem (jeżeli tak to istnieje token)
     * Jeżeli hasło wygasło, przekierowuje na skonfigurowaną sćieżkę.
     *
     * @param GetResponseEvent $event
     *
     * @return type
     */
    public function onCheckExpired(GetResponseEvent $event)
    {
        if (!$this->passwordExpires || !$event->isMasterRequest()) {
            return;
        }

        /*
         * IS_AUTHENTICATED_FULLY - logowanie przez formularz
         * IS_AUTHENTICATED_REMEMBERED - logowanie przez ciasteczko lub formularz
         */
        if ($this->tokenStorage->getToken() && $this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            /* @var $user UserInterface */

            if ($event->getRequest()->get('_route') !== $this->redirectRoute) {
                $user = $this->tokenStorage->getToken()->getUser();
                if ($user->isPasswordExpired()) {
                    $response = new RedirectResponse($this->router->generate($this->redirectRoute, array('id' => $user->getId())));
                    $event->setResponse($response);
                }
            }
        }
    }
}
