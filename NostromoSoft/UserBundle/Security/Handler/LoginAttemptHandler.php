<?php

/*
 * Copyright (c) 2016 by Nostromo Soft.
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.<br> *
 */

namespace NostromoSoft\UserBundle\Security\Handler;

use DateTime;
use NostromoSoft\Base\Modules\BaseBundle\Manager\CrudObjectManager;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\CustomAuthenticationFailureHandler;
use Symfony\Component\Security\Http\Authentication\CustomAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;

/**
 * Klasa odpowiedzialna za logowanie udanych i nieudanych prób logowania na danym firewallu.
 *
 * Wsytarczy dodać stosowną konfigurację w security.yml
 *
 * firewalls:
 *        _firewall_:
 *              failure_handler:  _service_
 *              success_handler:  _service_
 *
 * Mechanizm komponentu security stworzy("udekoruje") instancję handlera przy pomocy klasy CustomAuthenticationSuccessHandler.
 * Do konstruktora której przekazana zostanie poniższa klasa jako "handler".
 * Interfejs AuthenticationSuccessHandlerInterface nie zaweira metody "setOptions", jednak klasa CustomAuthenticationSuccessHandler
 * sprawdza czy jest ona dostępna w klasie implementującej ten interfejs i przekazuje w ten sposób konfigurację firewalla do naszego handlera.
 *
 * To samo dzieje się odpowiednio w przypadku "failure"
 *
 * @see CustomAuthenticationSuccessHandler
 * @see CustomAuthenticationFailureHandler
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class LoginAttemptHandler implements AuthenticationFailureHandlerInterface, AuthenticationSuccessHandlerInterface
{
    protected $loginAttemptManager;
    protected $userManager;
    protected $options;
    protected $httpUtils;
    protected $providerKey;
    protected $defaultOptions = array(
        'always_use_default_target_path' => false,
        'default_target_path' => '/',
        'login_path' => '/login',
        'target_path_parameter' => '_target_path',
        'use_referer' => false,
        'failure_path' => null,
        'failure_forward' => false,
        'failure_path_parameter' => '_failure_path',
    );

    public function __construct(CrudObjectManager $loginAttemptManager, CrudObjectManager $userManager, HttpUtils $httpUtils)
    {
        $this->loginAttemptManager = $loginAttemptManager;
        $this->userManager = $userManager;
        $this->httpUtils = $httpUtils;
    }

    /**
     * Funkcja wykorzystywana przez klasy CustomAuthentication*Handler będące proxy pomiędzy mechanizmem autentykacji
     * a zewnętrznymi "handlerami", przekazuje do handlera domyślne opcje konfiguracyjne z klasy AbstractFactory.
     *
     * @see AbstractFactory
     * @see CustomAuthenticationSuccessHandler
     * @see CustomAuthenticationFailureHandler
     *
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = array_merge($this->defaultOptions, $options);
    }

    /**
     * Funkcja wykorzystywana przez klasy CustomAuthentication*Handler będące proxy pomiędzy mechanizmem autentykacji
     * a zewnętrznymi "handlerami", przekazuje do handlera nazwę firewalla tak, żeby można było wyciągnąć ścieżkę przekierowania
     * z jego konfiguracji zamiast z konfiguracji samego handlera.
     *
     * @see CustomAuthenticationSuccessHandler
     * @see CustomAuthenticationFailureHandler
     *
     * @param string $providerKey
     */
    public function setProviderKey($providerKey)
    {
        $this->providerKey = $providerKey;
    }

    /**
     * Funkcja zapożyczona z domyślnego handlera, zapisuje wyjątek w sesji, i ustala ścieżkę przekierowania.
     * Dodatkowo Zapisuje zdarzenie logowania w bazie.
     *
     * @see DefaultAuthenticationFailureHandler
     *
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($exception instanceof BadCredentialsException) {
            $username = $exception->getToken()->getUser();
            $user = $this->userManager->getByUsername($username);
            if ($user) {
                $this->logAttempt($user, false);
            }
        }

        if ($failureUrl = $request->get($this->options['failure_path_parameter'], null, true)) {
            $this->options['failure_path'] = $failureUrl;
        }

        if (null === $this->options['failure_path']) {
            $this->options['failure_path'] = $this->options['login_path'];
        }

        if ($this->options['failure_forward']) {
            $subRequest = $this->httpUtils->createRequest($request, $this->options['failure_path']);
            $subRequest->attributes->set(Security::AUTHENTICATION_ERROR, $exception);

            return $this->httpKernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
        }

        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

        return $this->httpUtils->createRedirectResponse($request, $this->options['failure_path']);
    }

    /**
     * Zapisuje zdarzenie logowania w bazie i ustala ścieżkę przekierowania.
     *
     * @param Request        $request
     * @param TokenInterface $token
     *
     * @return type
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user = $token->getUser();

        $this->logAttempt($user, true);

        return $this->httpUtils->createRedirectResponse($request, $this->determineTargetUrl($request));
    }

    /**
     * Loguje zdarzenie logowania w bazie.
     *
     * @param $user
     * @param $isSuccessfull
     *
     * @internal param AuthenticationException $exception
     */
    protected function logAttempt($user, $isSuccessfull)
    {
        $la = $this->loginAttemptManager->create();
        $la->setLoggedAt(new DateTime());
        $la->setIsSuccessfull($isSuccessfull);
        $la->setUserId($user->getId());

        $this->loginAttemptManager->log($la);
    }

    /**
     * Funkcja zapożyczona z klasy DefaultAuthenticationSuccessHandler, ustala ścieżkę przekierowania po akcji logowania.
     *
     * @see DefaultAuthenticationSuccessHandler
     *
     * @param Request $request
     *
     * @return string ścieżka przekierowania
     */
    protected function determineTargetUrl(Request $request)
    {
        if ($this->options['always_use_default_target_path']) {
            return $this->options['default_target_path'];
        }

        if ($targetUrl = $request->get($this->options['target_path_parameter'], null, true)) {
            return $targetUrl;
        }

        if (null !== $this->providerKey && $targetUrl = $request->getSession()->get('_security.'.$this->providerKey.'.target_path')) {
            $request->getSession()->remove('_security.'.$this->providerKey.'.target_path');

            return $targetUrl;
        }

        if ($this->options['use_referer'] && ($targetUrl = $request->headers->get('Referer')) && $targetUrl !== $this->httpUtils->generateUri($request, $this->options['login_path'])) {
            return $targetUrl;
        }

        return $this->options['default_target_path'];
    }
}
