<?php

namespace NostromoSoft\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use NostromoSoft\UserBundle\Security\Factory\FormLoginFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class NomaSolutionsUserBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new FormLoginFactory());
    }
}
