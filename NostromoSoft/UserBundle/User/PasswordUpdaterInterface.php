<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\User;

use NostromoSoft\UserBundle\Model\UserInterface;

/**
 * Interfejsk klasy obsługującej zdarzenie zmiany hasła.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
interface PasswordUpdaterInterface
{
    /**
     * Wywołuje proces aktualizacji hasła.
     *
     * @param UserInterface $user
     *
     * @return
     */
    public function updatePassword(UserInterface $user);
}
