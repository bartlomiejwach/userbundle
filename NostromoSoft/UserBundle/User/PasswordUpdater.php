<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\User;

use DateTime;
use NostromoSoft\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * Klasa odpowiedzialna za zmienianie hasła i aktualizowanie daty wygaśnięcia hasła.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class PasswordUpdater implements PasswordUpdaterInterface
{
    protected $encoderFactory;
    protected $passwordExpires;
    protected $passwordLifetime;

    public function __construct(EncoderFactoryInterface $encoderFactory, $passwordExpires, $passwordLifeTime)
    {
        $this->encoderFactory = $encoderFactory;
        $this->passwordExpires = $passwordExpires;
        $this->passwordLifetime = intval($passwordLifeTime);
    }

    /**
     * {@inheridDoc}.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function updatePassword(UserInterface $user)
    {
        if (0 !== strlen($password = $user->getPlainPassword())) {
            $this->encodePassword($user);
            $this->updatePasswordExpirationData($user);

            $user->setPasswordChangedAt(new \DateTime());
            $user->eraseCredentials();
        }

        return $user;
    }

    /**
     * Koduje hasło użytkownika.
     *
     * @param UserInterface $user
     */
    protected function encodePassword(UserInterface &$user)
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        $encodedPassword = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        $user->setPassword($encodedPassword);
    }

    /**
     * Aktualizuje dane związane z wygasaniem i odzyskiwaniem hasła.
     *
     * @see http://php.net/manual/pl/datetime.settimestamp.php#refsect1-datetime.settimestamp-notes
     *
     * @param UserInterface $user
     */
    protected function updatePasswordExpirationData(UserInterface &$user)
    {
        if ($this->passwordExpires) {
            $expirationTimeStamp = time();

            /*
             * jeżeli zmieniamy hasło istniejącego użytkownika, przesuwamy datę
             * jeżeli zmieniamy hasło nowego, wymuszamy zmianę hasła po zalogowaniu poprzez
             * pozostawienia daty wygaśnięcia jako time()
             */
            if ($user->getId()) {
                $expirationTimeStamp += $this->passwordLifetime * 24 * 60 * 60;
            }

            $user->setPasswordExpiresAt(new DateTime("@$expirationTimeStamp"));
        }
    }
}
