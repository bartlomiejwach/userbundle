<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All rights reserved.
 */

namespace NostromoSoft\UserBundle\Controller;

use NostromoSoft\Base\Modules\BaseBundle\Controller\BaseCrudController;

/**
 * Klasa dodająca integrację notyfikacji do kontrollera.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
abstract class NotificationsAwareController extends BaseCrudController
{
    /**
     * Dodaje powiadomienie przy pomocy notificationManagera jeżeli notificationBundle jest uruchomiony.
     *
     * @param string $message wiadomość
     * @param string $type    typ wiadomości
     */
    private function notify($message, $type)
    {
        if ($this->container->has('ns_notification.notification_manager')) {
            $notificationManager = $this->get('ns_notification.notification_manager');
            switch ($type) {
                case 'error':
                    $notificationManager->addError($message);
                    break;
                case 'success':
                    $notificationManager->addSuccess($message);
                    break;
                case 'info':
                default:
                    $notificationManager->addInfo($message);
                    break;
            }
        }
    }

    /**
     * Dodaje powiadomienie o błędzie.
     *
     * @param string $message
     */
    protected function notifyError($message)
    {
        $this->notify($message, 'error');
    }

    /**
     * Dodaje powiadomienie z informacją.
     *
     * @param string $message
     */
    protected function notifyInfo($message)
    {
        $this->notify($message, 'info');
    }
}
