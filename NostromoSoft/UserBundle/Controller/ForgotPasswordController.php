<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\Controller;

use InvalidArgumentException;
use NostromoSoft\UserBundle\Manager\UserFormManager;
use NostromoSoft\UserBundle\Manager\UserManager;
use NostromoSoft\UserBundle\PasswordReset\PasswordReset;
use NostromoSoft\UserBundle\PasswordReset\PasswordResetHandlerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 *
 * @Route("/przypomnienie-hasla")
 */
class ForgotPasswordController extends Controller
{
    /**
     * Formularz przypomnienia.
     *
     * @Route("/", name="forgot_request")
     * @Method({"GET"})
     *
     * @param Request $request
     *
     * @return
     */
    public function requestAction(Request $request)
    {
        $userFormManager = $this->get('nostromo_soft_user.user_form_manager');
        $form = $userFormManager->createPasswordResetRequestForm('forgot_handle');
        $error = null;

        if ($request->getSession()->has(PasswordReset::WRONG_USERNAME)) {
            $error = $request->getSession()->get(PasswordReset::WRONG_USERNAME);
            $request->getSession()->remove(PasswordReset::WRONG_USERNAME);
        }

        return $this->render('NostromoSoftSystemGuiBundle:User:ForgotPassword/request.html.twig', array(
                    'form' => $form->createView(),
                    'error' => $error,
        ));
    }

    /**
     * Obsłużenie zapytania o zmianę hasła.
     *
     * @Route("/", name="forgot_handle")
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return
     */
    public function sendEmailAction(Request $request)
    {
        $userFormManager = $this->get('nostromo_soft_user.user_form_manager');
        $form = $userFormManager->createPasswordResetRequestForm('forgot_handle');
        /* @var $handler PasswordResetHandlerInterface */
        $handler = $this->get($this->container->getParameter('nostromo_soft_user.password_reset.handler'));
        $response = $this->redirectToRoute('forgot_request');

        $form->handleRequest($request);

        try {
            $response = $handler->handleRequest($request, $form);
        } catch (InvalidArgumentException $ex) {
            $response = $this->redirectToRoute('forgot_request');
            $request->getSession()->set(PasswordReset::WRONG_USERNAME, $ex->getMessage());
        }

        return $response;
    }

    /**
     * Info o wysłaniu maila.
     *
     * @Route("/potwierdzenie-wyslania", name="forgot_email_sent")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function checkEmailAction(Request $request)
    {
        $email = $request->query->get('email');

        if (empty($email)) {
            return new RedirectResponse($this->generateUrl('user_resetting_request'));
        }

        return $this->render('NostromoSoftSystemGuiBundle:User:ForgotPassword/checkEmail.html.twig', array(
                    'email' => $email,
        ));
    }

    /**
     * Reset hasła.
     *
     * @Route("/zmiana-hasla/{token}", name="forgot_reset")
     *
     * @param Request $request
     * @param $token
     *
     * @return
     *
     * @throws NotFoundHttpException
     */
    public function resetAction(Request $request, $token)
    {
        /* @var $userManager UserManager */
        $userManager = $this->get('nostromo_soft_user.user_manager');
        /* @var $userManager UserFormManager */
        $userFormManager = $this->get('nostromo_soft_user.user_form_manager');
        $user = $userManager->getByField('confirmationToken', $token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        $form = $userFormManager->createPasswordResetForm($user, 'forgot_handle');

        $form->handleRequest($request);
        if ($request->getMethod() == 'POST' && $form->isValid()) {
            $userManager->changeForgotPassword($user);

            return $this->redirectToRoute('forgot_confirm');
        }

        return $this->render('NostromoSoftSystemGuiBundle:User:ForgotPassword/reset.html.twig', array(
                    'token' => $token,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Info o wysłaniu maila.
     *
     * @Route("/potwierdzenie-zmiany-hasla", name="forgot_confirm")
     *
     * @param Request $request
     *
     * @return
     */
    public function confirmAction(Request $request)
    {
        return $this->render('NostromoSoftSystemGuiBundle:User:ForgotPassword/confirm.html.twig');
    }
}
