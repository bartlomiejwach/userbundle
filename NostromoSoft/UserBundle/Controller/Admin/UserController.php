<?php

/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\Controller\Admin;

use NostromoSoft\UserBundle\Controller\NotificationsAwareController;
use NostromoSoft\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller CRUD dla użytkownika.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 *
 * @Route("/admin/uzytkownicy")
 */
class UserController extends NotificationsAwareController
{
    protected function prepareObjects()
    {
        $this->objectManager = $this->get('nostromo_soft_user.user_manager');
        $this->objectFormManager = $this->get('nostromo_soft_user.user_form_manager');
    }

    /**
     * NSTODO: Wymagane przesłonięcie, bo się coś krzaczy z succesRoute.
     */
    protected function successRedirect()
    {
        $request = $this->getRequest();

        $parameters = array(
            'page' => $request->getSession()->get($this->successRoute.'_page'),
        );

        return $this->redirect($this->generateUrl('user_list', $parameters));
    }

    /**
     * @Route("/", name="user_list")
     */
    public function listAction()
    {
        $viewParameters = parent::pagedListAction();
        $viewParameters['passwordExpires'] = $this->container->getParameter('nostromo_soft_user.user.password_expires');

        return $this->render('NostromoSoftSystemGuiBundle:Admin:User/list.html.twig', $viewParameters);
    }

    /**
     * @Route("/dodaj", name="user_new")
     * @Template()
     *
     * @param Request $request
     * @param $_route
     *
     * @return
     */
    public function newAction(Request $request, $_route)
    {
        $result = parent::newAction($request, $_route);

        if ($result instanceof RedirectResponse) {
            $this->notifyInfo('Dane zostały zapisane');

            return $result;
        }

        return $this->render('NostromoSoftSystemGuiBundle:Admin:User/new.html.twig', $result);
    }

    /**
     * @Route("/{id}/edytuj", name="user_edit")
     * @Template()
     *
     * @param Request $request
     * @param $id
     * @param $_route
     *
     * @return
     */
    public function editAction(Request $request, $id, $_route)
    {
        $result = parent::editAction($request, $id, $_route);

        if ($result instanceof RedirectResponse) {
            $this->notifyInfo('Dane zostały zapisane');

            return $result;
        }

        return $this->render('NostromoSoftSystemGuiBundle:Admin:User/edit.html.twig', $result);
    }

    /**
     * @Route("/{id}/zmien-haslo", name="user_change_password")
     * @Template()
     *
     * @param Request $request
     * @param $id
     * @param $_route
     *
     * @return
     */
    public function changePasswordAction(Request $request, $id, $_route)
    {
        $this->prepareObjects();

        $object = $this->objectManager->getById($id);

        if (!$object) {
            throw $this->createNotFoundException();
        }

        $form = $this->objectFormManager->createChangePasswordForm($_route, array('id' => $id), $object);

        if ($this->objectFormManager->handleFormRequest($form, $request)) {
            $this->notifyInfo('Dane zostały zapisane');

            return $this->successRedirect();
        }

        $result = array(
            'object' => $object,
            'form' => $form->createView(),
        );

        return $this->render('NostromoSoftSystemGuiBundle:Admin:User/changePassword.html.twig', $result);
    }

    /**
     * @Route("/{id}/usun", name="user_delete")
     *
     * @param $id
     *
     * @return
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        if ($response instanceof RedirectResponse) {
            $this->notifyInfo('Użytkownik został usunięty');
        }

        return $response;
    }

    /**
     * Zablokowanie użytkownika.
     *
     * @Route("/{id}/zablokuj", name="user_block")
     *
     * @param $id
     *
     * @return Response
     */
    public function blockAction($id)
    {
        return $this->setLockedFlagTo($id, true);
    }

    /**
     * Obdlobkowanie użytkownika.
     *
     * @Route("/{id}/odblokuj", name="user_unblock")
     *
     * @param $id
     *
     * @return Response
     */
    public function unblockAction($id)
    {
        return $this->setLockedFlagTo($id, false);
    }

    /**
     * Pomocnicza funckja zmieniająca flagę "zablokowany".
     *
     * @param int  $id   Id usera
     * @param bool $flag wartość falgi
     *
     * @return Response
     *
     * @throws NotFoundException
     */
    protected function setLockedFlagTo($id, $flag)
    {
        // uzytkownik nie moze od/zablokowac sam siebie
        if ($id == $this->getUser()->getId()) {
            return $this->redirectToRoute('user_list');
        }

        $this->prepareObjects();

        /* @var $object UserInterface */
        $object = $this->objectManager->getById($id);

        if (!$object) {
            throw $this->createNotFoundException();
        }

        $object->setIsLocked($flag);
        $this->objectManager->update($object);

        $this->notifyInfo(sprintf('Użytkownik został %s', $flag ? 'zablokowany' : 'odblokowany'));

        return $this->redirectToRoute('user_list');
    }
}
