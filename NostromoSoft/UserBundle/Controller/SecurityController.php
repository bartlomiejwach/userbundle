<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends Controller
{
    /**
     * Akcja pokazująca formularz logowania.
     *
     * @Route("/login",name="user_login")
     *
     * @param Request $request
     *
     * @return
     */
    public function loginAction(Request $request)
    {
        $crsfProvider = $this->container->has('form.csrf_provider') ? $this->container->get('form.csrf_provider') : null;
        $authenticationUtils = $this->get('security.authentication_utils');
        $authenticationProvider = $this->get('nostromo_soft_user.authentication_provider.admin_area');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $csrfToken = $crsfProvider ? $crsfProvider->generateCsrfToken('authenticate') : null;

        return $this->render(
                'NostromoSoftSystemGuiBundle:Security:login.html.twig', array(
                'last_username' => $lastUsername,
                'error' => $error,
                'csrf_token' => $csrfToken,
                'login_field' => $authenticationProvider->getLoginFieldName(),
                'remember_me' => $authenticationProvider->isRememberMeAware(),
                'reset_password' => $authenticationProvider->getResetPasswordPath(),
        ));
    }

    /**
     * Akcja wykonująca dodatkowe sprawdzanie usera.
     * Nie jest wykonywana poneważ kontrola jest przekazywana do komponentu security.
     *
     * @see http://symfony.com/doc/current/cookbook/security/form_login_setup.html
     *
     * @Route("/login_check",name="user_check")
     */
    public function checkAction()
    {
    }

    /**
     * Akcja odpowiedzialna za wylogowanie
     * Nie jest wykonywana poneważ kontrola jest przekazywana do komponentu security.
     *
     * @see http://symfony.com/doc/current/cookbook/security/form_login_setup.html
     *
     * @Route("/logout",name="user_logout")
     */
    public function logoutAction()
    {
    }
}
