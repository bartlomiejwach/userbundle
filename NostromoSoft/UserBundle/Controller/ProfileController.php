<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\Controller;

use NostromoSoft\UserBundle\Manager\LoginAttemptManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller zaweirający akcje związane z własnym kontem użytkownika.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 *
 * @Route("/profil")
 */
class ProfileController extends NotificationsAwareController
{
    protected function prepareObjects()
    {
        $this->objectManager = $this->get('nostromo_soft_user.user_manager');
        $this->objectFormManager = $this->get('nostromo_soft_user.user_form_manager');
    }

    /**
     * Zwraca przekierowanie na frontController.
     *
     * @return Response
     */
    protected function successRedirect()
    {
        return $this->redirectToRoute($this->container->getParameter('nostromo_soft_user.user.change_password_success_route'));
    }

    /**
     * Zmiana hasła do swojego konta.
     *
     * @Route("/zmien-haslo", name="user_profile_change_password")
     *
     * @param Request $request
     * @param $_route
     *
     * @return Response
     */
    public function changePasswordAction(Request $request, $_route)
    {
        $this->prepareObjects();

        $user = $this->getUser();

        $form = $this->objectFormManager->createChangeProfilePasswordForm($_route, array(), $user);

        if ($this->objectFormManager->handleFormRequest($form, $request)) {
            $this->notifyInfo('Dane zostały zapisane');

            return $this->successRedirect();
        }

        return $this->render('NostromoSoftSystemGuiBundle:User:Profile/changePassword.html.twig', array(
                'object' => $user,
                'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/", name="user_profile_edit")
     *
     * @param Request $request
     * @param $_route
     *
     * @return Response
     */
    public function profileEditAction(Request $request, $_route)
    {
        $this->prepareObjects();
        $user = $this->getUser();

        $form = $this->objectFormManager->createProfileForm($_route, array(), $user);

        if ($this->objectFormManager->handleFormRequest($form, $request)) {
            $this->notifyInfo('Dane zostały zapisane');

            return $this->successRedirect();
        }

        return $this->render('NostromoSoftSystemGuiBundle:User:Profile/profileEdit.html.twig', array(
                'object' => $user,
                'form' => $form->createView(),
        ));
    }

    /**
     * Renderuje widget z listą logowań, za pomocą parametru log_length można manipulować długością loga(domyślnie: 10).
     *
     * @param Request $request
     *
     * @return Response
     */
    public function loginHistoryWidgetAction(Request $request)
    {
        /* @var $loginLogger LoginAttemptManager */
        $loginLogger = $this->get('nostromo_soft_user.login_attempt_manager');
        $logLength = $request->get('log_length', 10);

        return $this->render('NostromoSoftSystemGuiBundle:User:Profile/Partial/loginHistoryWidget.html.twig', array('log' => $loginLogger->getLogForUser($this->getUser(), $logLength)));
    }
}
