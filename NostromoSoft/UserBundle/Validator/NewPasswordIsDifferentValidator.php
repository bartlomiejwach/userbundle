<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\Validator;

use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Walidator dla NewPasswordIsDifferent.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 */
class NewPasswordIsDifferentValidator extends ConstraintValidator
{
    protected $encoderFactory;

    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    public function validate($user, Constraint $constraint)
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        $oldPassword = $user->getPassword();

        if ($encoder->isPasswordValid($oldPassword, $user->getPlainPassword(), $user->getSalt())) {
            $this->context->buildViolation($constraint->message)
                ->atPath('plainPassword')
                ->addViolation();
        }
    }
}
