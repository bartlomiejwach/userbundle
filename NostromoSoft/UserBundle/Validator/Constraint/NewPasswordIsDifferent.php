<?php
/*
 * Copyright (c) 2016 by Nostromo Soft
 * This software is the proprietary information of Nostromo Soft.
 *
 * All Right Reserved.
 */

namespace NostromoSoft\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint zapewniający, że nowe hasło rózni się od poprzedniego.
 *
 * @author Bartłomiej Wach <bartlomiej.wach@yahoo.pl>
 *
 * @Annotation
 */
class NewPasswordIsDifferent extends Constraint
{
    public $message = 'Nowe hasło musi różnić się od poprzedniego';

    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return 'new_password_is_different';
    }
}
